﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;

namespace IdFriends
{
    static class Program
    {
        public static string UserID = "0";
        // your facebook application ID for ID friends
        public static string ApplicationId = "382569058543016";
        //TODO: add secret key security measures if required.
        public static string ApplicationSecret = "";
        public const string ExtendedPermissionsNeeded = "email,user_friends";
        public static string AccessToken;
        public static bool Authorized;
        public static string CurrentName;
        public static string CurrentEmail;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
