﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Emgu.CV.UI;
using Emgu.CV;
using Emgu.CV.Face;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;

using System.IO;
using System.Xml;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Drawing.Imaging;
using System.Drawing;

/// <summary>
/// Desingned to remove the training a EigenObjectRecognizer code from the main form
/// </summary>
public class ClassiferTrainer: IDisposable
{
    #region Variables

    //Eigen
    //EigenObjectRecognizer recognizer;
    FaceRecognizer recognizer;

    //training variables
    List<Image<Gray, byte>> trainingImages = new List<Image<Gray, byte>>();//Images
    //TODO: see if this can be combined in Ditionary format this will remove support for old data
    List<string> nameList = new List<string>(); //labels
    List<int> idList = new List<int>();
    int contTrain, labelCount;
    public float eigenDistance { private set; get; } = 0;
    public string eigenLabel { private set; get; }
    int eigenThreshold = 2000;

    //Class Variables
    string error;
    public bool isTrained { private set; get; } = false;

    public string recognizerType = "EMGU.CV.EigenFaceRecognizer";
    #endregion

    #region Constructors
    /// <summary>
    /// Default Constructor, Looks in (Application.StartupPath + "\\TrainedFaces") for traing data.
    /// </summary>
    public ClassiferTrainer()
    {
        isTrained = LoadTrainingData(Application.StartupPath + "\\TrainedFaces");
    }

    /// <summary>
    /// Takes String input to a different location for training data
    /// </summary>
    /// <param name="Training_Folder"></param>
    public ClassiferTrainer(string Training_Folder)
    {
        isTrained = LoadTrainingData(Training_Folder);
    }
    #endregion

    #region Public
    /// <summary>
    /// Retrains the recognizer witout resetting variables like recognizer type.
    /// </summary>
    /// <returns></returns>
    public bool Retrain()
    {
        return isTrained = LoadTrainingData(Application.StartupPath + "\\TrainedFaces");
    }
    /// <summary>
    /// Retrains the recognizer witout resetting variables like recognizer type.
    /// Takes String input to a different location for training data.
    /// </summary>
    /// <returns>bool</returns>
    public bool Retrain(string Training_Folder)
    {
        return isTrained = LoadTrainingData(Training_Folder);
    }

    /// <summary>
    /// Recognise a Grayscale Image using the trained Eigen Recogniser
    /// </summary>
    /// <param name="inputImage"></param>
    /// <returns></returns>
    public string Recognise(Image<Gray, byte> inputImage, int eigenThreshold = -1)
    {
        if (isTrained)
        {
            FaceRecognizer.PredictionResult ER = recognizer.Predict(inputImage);

            if (ER.Label == -1)
            {
                eigenLabel = "Unknown";
                eigenDistance = 0;
                return eigenLabel;
            }
            else
            {
                eigenLabel = nameList[ER.Label];
                eigenDistance = (float)ER.Distance;
                if (eigenThreshold > -1) this.eigenThreshold = eigenThreshold;

                //Only use the post threshold rule if we are using an Eigen Recognizer 
                //since Fisher and LBHP threshold set during the constructor will work correctly 
                switch (recognizerType)
                {
                    case ("EMGU.CV.EigenFaceRecognizer"):
                            if (eigenDistance > this.eigenThreshold) return eigenLabel;
                            else return "Unknown";
                    case ("EMGU.CV.LBPHFaceRecognizer"):
                    case ("EMGU.CV.FisherFaceRecognizer"):
                    default:
                            return eigenLabel; //the threshold set in training controls unknowns
                }
            }
        }
        else return "";
    }

    /// <summary>
    /// Saves the trained Eigen Recogniser to specified location
    /// </summary>
    /// <param name="filename"></param>
    public void SaveEigenRecognizer(string filename)
    {
        recognizer.Save(filename);

        //save label data as this isn't saved with the network
        string direct = Path.GetDirectoryName(filename);
        FileStream Label_Data = File.OpenWrite(direct + "/Labels.xml");
        using (XmlWriter writer = XmlWriter.Create(Label_Data))
        {
            writer.WriteStartDocument();
            writer.WriteStartElement("Labels_For_Recognizer_sequential");
            for (int i = 0; i < nameList.Count; i++)
            {
                writer.WriteStartElement("LABEL");
                writer.WriteElementString("POS", i.ToString());
                writer.WriteElementString("NAME", nameList[i]);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndDocument();
        }
        Label_Data.Close();
    }

    /// <summary>
    /// Loads the trained Eigen Recogniser from specified location
    /// </summary>
    /// <param name="filename"></param>
    public void LoadEigenRecognizer(string filename)
    {
        //Lets get the recogniser type from the file extension
        string ext = Path.GetExtension(filename);
        switch (ext)
        {
            case (".LBPH"):
                recognizerType = "EMGU.CV.LBPHFaceRecognizer";
                recognizer = new LBPHFaceRecognizer(1, 8, 8, 8, 100);//50
                break;
            case (".FFR"):
                recognizerType = "EMGU.CV.FisherFaceRecognizer";
                recognizer = new FisherFaceRecognizer(0, 3500);//4000
                break;
            case (".EFR"):
                recognizerType = "EMGU.CV.EigenFaceRecognizer";
                recognizer = new EigenFaceRecognizer(80, double.PositiveInfinity);
                break;
        }

        //introduce error checking
        recognizer.Load(filename);

        //Now load the labels
        string direct = Path.GetDirectoryName(filename);
        nameList.Clear();
        if (File.Exists(direct + "/Labels.xml"))
        {
            FileStream filestream = File.OpenRead(direct + "/Labels.xml");
            long filelength = filestream.Length;
            byte[] xmlBytes = new byte[filelength];
            filestream.Read(xmlBytes, 0, (int)filelength);
            filestream.Close();

            MemoryStream xmlStream = new MemoryStream(xmlBytes);

            using (XmlReader xmlreader = XmlTextReader.Create(xmlStream))
            {
                while (xmlreader.Read())
                {
                    if (xmlreader.IsStartElement())
                    {
                        switch (xmlreader.Name)
                        {
                            case "NAME":
                                if (xmlreader.Read())
                                {
                                    nameList.Add(xmlreader.Value.Trim());
                                }
                                break;
                        }
                    }
                }
            }
            contTrain = labelCount;
        }
        isTrained = true;
    }

        /// <summary>
    /// Dispose of Class call Garbage Collector
    /// </summary>
    public void Dispose()
    {
        recognizer = null;
        trainingImages = null;
        nameList = null;
        error = null;
        GC.Collect();
    }

    #endregion

    #region Private
    /// <summary>
    /// Loads the traing data given a (string) folder location
    /// </summary>
    /// <param name="Folder_location"></param>
    /// <returns></returns>
    private bool LoadTrainingData(string Folder_location)
    {
        if (File.Exists(Folder_location +"\\TrainedLabels.xml"))
        {
            try
            {
                //message_bar.Text = "";
                nameList.Clear();
                idList.Clear();
                trainingImages.Clear();
                FileStream filestream = File.OpenRead(Folder_location + "\\TrainedLabels.xml");
                long filelength = filestream.Length;
                byte[] xmlBytes = new byte[filelength];
                filestream.Read(xmlBytes, 0, (int)filelength);
                filestream.Close();

                MemoryStream xmlStream = new MemoryStream(xmlBytes);

                using (XmlReader xmlreader = XmlTextReader.Create(xmlStream))
                {
                    while (xmlreader.Read())
                    {
                        if (xmlreader.IsStartElement())
                        {
                            switch (xmlreader.Name)
                            {
                                case "NAME":
                                    if (xmlreader.Read())
                                    {
                                        idList.Add(nameList.Count); //0, 1, 2, 3....
                                        nameList.Add(xmlreader.Value.Trim());
                                        labelCount += 1;
                                    }
                                    break;
                                case "FILE":
                                    if (xmlreader.Read())
                                    {
                                        //PROBLEM HERE IF TRAININGG MOVED
                                        trainingImages.Add(new Image<Gray, byte>(Application.StartupPath + "\\TrainedFaces\\" + xmlreader.Value.Trim()));
                                    }
                                    break;
                            }
                        }
                    }
                }
                contTrain = labelCount;

                if (trainingImages.ToArray().Length != 0)
                {

                    //Eigen face recognizer
                    //Parameters:	
                    //      num_components – The number of components (read: Eigenfaces) kept for this Prinicpal 
                    //          Component Analysis. As a hint: There’s no rule how many components (read: Eigenfaces) 
                    //          should be kept for good reconstruction capabilities. It is based on your input data, 
                    //          so experiment with the number. Keeping 80 components should almost always be sufficient.
                    //
                    //      threshold – The threshold applied in the prediciton. This still has issues as it work inversly to LBH and Fisher Methods.
                    //          if you use 0.0 recognizer.Predict will always return -1 or unknown if you use 5000 for example unknow won't be reconised.
                    //          As in previous versions I ignore the built in threhold methods and allow a match to be found i.e. double.PositiveInfinity
                    //          and then use the eigen distance threshold that is return to elliminate unknowns. 
                    //
                    //NOTE: The following causes the confusion, sinc two rules are used. 
                    //--------------------------------------------------------------------------------------------------------------------------------------
                    //Eigen Uses
                    //          0 - X = unknown
                    //          > X = Recognised
                    //
                    //Fisher and LBPH Use
                    //          0 - X = Recognised
                    //          > X = Unknown
                    //
                    // Where X = Threshold value


                    switch (recognizerType)
                    {
                        case ("EMGU.CV.LBPHFaceRecognizer"):
                            recognizer = new LBPHFaceRecognizer(1, 8, 8, 8, 100);//50
                            break;
                        case ("EMGU.CV.FisherFaceRecognizer"):
                            recognizer = new FisherFaceRecognizer(0, 3500);//4000
                            break;
                        case("EMGU.CV.EigenFaceRecognizer"):
                        default:
                            recognizer = new EigenFaceRecognizer(80, double.PositiveInfinity);
                            break;
                    }

                    recognizer.Train(trainingImages.ToArray(), idList.ToArray());
                    // Recognizer_Type = recognizer.GetType();
                    // string v = recognizer.ToString(); //EMGU.CV.FisherFaceRecognizer || EMGU.CV.EigenFaceRecognizer || EMGU.CV.LBPHFaceRecognizer

                    return true;
                }
                else return false;
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                return false;
            }
        }
        else return false;
    }

    #endregion
}

