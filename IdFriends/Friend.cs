﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdFriends
{
    public class Friend
    {
        public string name {get;set;}
        public string id {get;set;}

        public Friend(string name, string id) {
            this.name = name;
            this.id = id;
        }
    }

    public class TaggableFriend
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public dynamic Picture { get; set; }
        

        public TaggableFriend(string name, string id,dynamic picture)
        {
            Name = name;
            ID = id;
            Picture = picture;
        }
    }
}
