﻿namespace IdFriends
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listFriends = new System.Windows.Forms.ListBox();
            this.lblFreindsLabel = new System.Windows.Forms.Label();
            this.lbNameLabel = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblSomething = new System.Windows.Forms.Label();
            this.imgFriendFace = new System.Windows.Forms.PictureBox();
            this.browserLogin = new System.Windows.Forms.WebBrowser();
            this.btnSwitch = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnFire = new System.Windows.Forms.Button();
            this.cboMagnitude = new System.Windows.Forms.ComboBox();
            this.lblMagnitude = new System.Windows.Forms.Label();
            this.imgCamera = new Emgu.CV.UI.ImageBox();
            this.btnTrain = new System.Windows.Forms.Button();
            this.pbxGrayFriend = new System.Windows.Forms.PictureBox();
            this.btnTrainFriend = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.imgFriendFace)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCamera)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxGrayFriend)).BeginInit();
            this.SuspendLayout();
            // 
            // listFriends
            // 
            this.listFriends.FormattingEnabled = true;
            this.listFriends.Location = new System.Drawing.Point(274, 13);
            this.listFriends.Name = "listFriends";
            this.listFriends.Size = new System.Drawing.Size(128, 134);
            this.listFriends.TabIndex = 0;
            // 
            // lblFreindsLabel
            // 
            this.lblFreindsLabel.AutoSize = true;
            this.lblFreindsLabel.Location = new System.Drawing.Point(224, 13);
            this.lblFreindsLabel.Name = "lblFreindsLabel";
            this.lblFreindsLabel.Size = new System.Drawing.Size(44, 13);
            this.lblFreindsLabel.TabIndex = 1;
            this.lblFreindsLabel.Text = "Friends:";
            // 
            // lbNameLabel
            // 
            this.lbNameLabel.AutoSize = true;
            this.lbNameLabel.Location = new System.Drawing.Point(39, 123);
            this.lbNameLabel.Name = "lbNameLabel";
            this.lbNameLabel.Size = new System.Drawing.Size(38, 13);
            this.lbNameLabel.TabIndex = 2;
            this.lbNameLabel.Text = "Name:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(83, 123);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(54, 13);
            this.lblName.TabIndex = 3;
            this.lblName.Text = "namehere";
            // 
            // lblSomething
            // 
            this.lblSomething.AutoSize = true;
            this.lblSomething.Location = new System.Drawing.Point(42, 149);
            this.lblSomething.Name = "lblSomething";
            this.lblSomething.Size = new System.Drawing.Size(79, 13);
            this.lblSomething.TabIndex = 4;
            this.lblSomething.Text = "labelSomething";
            // 
            // imgFriendFace
            // 
            this.imgFriendFace.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.imgFriendFace.Location = new System.Drawing.Point(42, 13);
            this.imgFriendFace.Name = "imgFriendFace";
            this.imgFriendFace.Size = new System.Drawing.Size(85, 91);
            this.imgFriendFace.TabIndex = 5;
            this.imgFriendFace.TabStop = false;
            // 
            // browserLogin
            // 
            this.browserLogin.Location = new System.Drawing.Point(12, 165);
            this.browserLogin.MinimumSize = new System.Drawing.Size(20, 20);
            this.browserLogin.Name = "browserLogin";
            this.browserLogin.Size = new System.Drawing.Size(470, 303);
            this.browserLogin.TabIndex = 6;
            this.browserLogin.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.LoginWebBrowserNavigated);
            // 
            // btnSwitch
            // 
            this.btnSwitch.Location = new System.Drawing.Point(418, 124);
            this.btnSwitch.Name = "btnSwitch";
            this.btnSwitch.Size = new System.Drawing.Size(75, 23);
            this.btnSwitch.TabIndex = 7;
            this.btnSwitch.Text = "Switch";
            this.btnSwitch.UseVisualStyleBackColor = true;
            this.btnSwitch.Click += new System.EventHandler(this.btnSwitch_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(499, 314);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 9;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.captureButtonClick);
            // 
            // btnUp
            // 
            this.btnUp.Location = new System.Drawing.Point(579, 366);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(75, 23);
            this.btnUp.TabIndex = 12;
            this.btnUp.Text = "Up";
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnDown
            // 
            this.btnDown.Location = new System.Drawing.Point(579, 395);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(75, 23);
            this.btnDown.TabIndex = 13;
            this.btnDown.Text = "Down";
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.Location = new System.Drawing.Point(660, 366);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(75, 23);
            this.btnLeft.TabIndex = 14;
            this.btnLeft.Text = "Left";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnRight
            // 
            this.btnRight.Location = new System.Drawing.Point(661, 395);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(75, 23);
            this.btnRight.TabIndex = 15;
            this.btnRight.Text = "Right";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(741, 366);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 16;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnFire
            // 
            this.btnFire.Location = new System.Drawing.Point(741, 395);
            this.btnFire.Name = "btnFire";
            this.btnFire.Size = new System.Drawing.Size(75, 23);
            this.btnFire.TabIndex = 17;
            this.btnFire.Text = "Fire";
            this.btnFire.UseVisualStyleBackColor = true;
            this.btnFire.Click += new System.EventHandler(this.btnFire_Click);
            // 
            // cboMagnitude
            // 
            this.cboMagnitude.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMagnitude.FormattingEnabled = true;
            this.cboMagnitude.Location = new System.Drawing.Point(579, 425);
            this.cboMagnitude.Name = "cboMagnitude";
            this.cboMagnitude.Size = new System.Drawing.Size(121, 21);
            this.cboMagnitude.TabIndex = 18;
            // 
            // lblMagnitude
            // 
            this.lblMagnitude.AutoSize = true;
            this.lblMagnitude.Location = new System.Drawing.Point(517, 428);
            this.lblMagnitude.Name = "lblMagnitude";
            this.lblMagnitude.Size = new System.Drawing.Size(60, 13);
            this.lblMagnitude.TabIndex = 19;
            this.lblMagnitude.Text = "Magnitude:";
            // 
            // imgCamera
            // 
            this.imgCamera.Location = new System.Drawing.Point(499, 13);
            this.imgCamera.Name = "imgCamera";
            this.imgCamera.Size = new System.Drawing.Size(394, 295);
            this.imgCamera.TabIndex = 2;
            this.imgCamera.TabStop = false;
            // 
            // btnTrain
            // 
            this.btnTrain.Location = new System.Drawing.Point(818, 314);
            this.btnTrain.Name = "btnTrain";
            this.btnTrain.Size = new System.Drawing.Size(75, 23);
            this.btnTrain.TabIndex = 25;
            this.btnTrain.Text = "Trainer";
            this.btnTrain.UseVisualStyleBackColor = true;
            this.btnTrain.Click += new System.EventHandler(this.btnTrain_Click);
            // 
            // pbxGrayFriend
            // 
            this.pbxGrayFriend.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pbxGrayFriend.Location = new System.Drawing.Point(133, 13);
            this.pbxGrayFriend.Name = "pbxGrayFriend";
            this.pbxGrayFriend.Size = new System.Drawing.Size(85, 91);
            this.pbxGrayFriend.TabIndex = 26;
            this.pbxGrayFriend.TabStop = false;
            // 
            // btnTrainFriend
            // 
            this.btnTrainFriend.Location = new System.Drawing.Point(176, 123);
            this.btnTrainFriend.Name = "btnTrainFriend";
            this.btnTrainFriend.Size = new System.Drawing.Size(75, 23);
            this.btnTrainFriend.TabIndex = 27;
            this.btnTrainFriend.Text = "Train";
            this.btnTrainFriend.UseVisualStyleBackColor = true;
            this.btnTrainFriend.Click += new System.EventHandler(this.btnTrainFriend_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 480);
            this.Controls.Add(this.btnTrainFriend);
            this.Controls.Add(this.pbxGrayFriend);
            this.Controls.Add(this.btnTrain);
            this.Controls.Add(this.imgCamera);
            this.Controls.Add(this.lblMagnitude);
            this.Controls.Add(this.cboMagnitude);
            this.Controls.Add(this.btnFire);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnRight);
            this.Controls.Add(this.btnLeft);
            this.Controls.Add(this.btnDown);
            this.Controls.Add(this.btnUp);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnSwitch);
            this.Controls.Add(this.browserLogin);
            this.Controls.Add(this.imgFriendFace);
            this.Controls.Add(this.lblSomething);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lbNameLabel);
            this.Controls.Add(this.lblFreindsLabel);
            this.Controls.Add(this.listFriends);
            this.Name = "MainForm";
            this.Text = "IdFriends";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.imgFriendFace)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCamera)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxGrayFriend)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listFriends;
        private System.Windows.Forms.Label lblFreindsLabel;
        private System.Windows.Forms.Label lbNameLabel;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblSomething;
        private System.Windows.Forms.PictureBox imgFriendFace;
        private System.Windows.Forms.WebBrowser browserLogin;
        private System.Windows.Forms.Button btnSwitch;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnFire;
        private System.Windows.Forms.ComboBox cboMagnitude;
        private System.Windows.Forms.Label lblMagnitude;
        private Emgu.CV.UI.ImageBox imgCamera;
        private System.Windows.Forms.Button btnTrain;
        private System.Windows.Forms.PictureBox pbxGrayFriend;
        private System.Windows.Forms.Button btnTrainFriend;
    }
}

