﻿using System;
using System.Net;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Facebook;

using Emgu.CV.UI;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;

using System.Xml;
using System.Runtime.InteropServices;
using System.Threading;
using System.Security.Principal;
using Microsoft.Win32.SafeHandles;


namespace IdFriends
{
    public partial class MainForm : Form
    {
        private string selectedUserID;
        private VideoCapture video = null;
        private bool isCapturing;
        FacebookClient fbClient;

        private string SelectedUserPictureURL
        {
            get
            {
                return "http://graph.facebook.com/" + selectedUserID + "/picture?type=large";
            }
        }

        private string friendlyApiString
        {
            get
            {
                if (selectedUserID == Program.UserID)
                    return @"\me";
                else
                    return @"\me\friends\" + selectedUserID;
            }
        }

        public MainForm()
        {
            InitializeComponent();

            // init launcher command dropdownlist
            string[] magnitudes = { "50", "100", "200", "400", "600", "800", "1000", "1200", "1400", "1600", "1800" };
            cboMagnitude.DataSource = magnitudes.ToList<String>();
            cboMagnitude.SelectedItem = "400";

            TryCamera();

            // navigate to facebook login url and login
            browserLogin.Navigate(GenerateLoginUrl());
        }

        // camera capture
        private void TryCamera()
        {
            try
            {
                video = new VideoCapture();
                //video.ImageGrabbed += ProcessFrame;
                Image<Bgr, Byte> image = video.QueryFrame().ToImage<Bgr, Byte>();
                imgCamera.Image = image;
                Application.Idle += new EventHandler(FrameGrabber_Standard);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
            //Image<Bgr, Byte> frame = Video.RetrieveBgrFrame();
            //Image<Bgr, byte> frame1 = new Image<Bgr, byte>()
            //var m = Video.QueryFrame();
            //Video.Retrieve(m);

            Image<Bgr, Byte> image = video.QueryFrame().ToImage<Bgr, Byte>();
            imgCamera.Image = image;
            //Image<Bgr, Byte> frame = Video.QueryFrame();

            // Image<Gray, Byte> grayFrame = frame.Convert<Gray, Byte>();
            // Image<Gray, Byte> smallGrayFrame = grayFrame.PyrDown();
            //Image<Gray, Byte> smoothedGrayFrame = smallGrayFrame.PyrUp();
            //Image<Gray, Byte> cannyFrame = smoothedGrayFrame.Canny(100, 60);


            //grayscaleImageBox.Image = grayFrame;
            //smoothedGrayscaleImageBox.Image = smoothedGrayFrame;
            //cannyImageBox.Image = cannyFrame;
        }

        private string GenerateLoginUrl()
        {
            dynamic parameters = new System.Dynamic.ExpandoObject();

            parameters.client_id = Program.ApplicationId;
            parameters.redirect_uri = "https://www.facebook.com/connect/login_success.html";
            parameters.response_type = "token";
            parameters.display = "popup";

            if (!string.IsNullOrWhiteSpace(Program.ExtendedPermissionsNeeded))
                parameters.scope = Program.ExtendedPermissionsNeeded;

            var fb = new FacebookClient();

            Uri loginUri = fb.GetLoginUrl(parameters);

            return loginUri.AbsoluteUri;
        }

        private void LoginWebBrowserNavigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            if (browserLogin.Visible)
            {
                fbClient = new FacebookClient();

                FacebookOAuthResult oauthResult;
                if (fbClient.TryParseOAuthCallbackUrl(e.Url, out oauthResult))
                {
                    if (oauthResult.IsSuccess)
                    {
                        Program.AccessToken = oauthResult.AccessToken;
                        Program.Authorized = true;
                    }
                    else
                    {
                        Program.AccessToken = "";
                        Program.Authorized = false;
                    }

                    if (Program.Authorized)
                    {
                        // login success
                        //fbClient = new FacebookClient(Program.AccessToken);
                        fbClient.AccessToken = Program.AccessToken;

                        dynamic me = fbClient.Get("me");
                        Program.CurrentName = me.name;
                        Program.CurrentEmail = me.email;
                        Program.UserID = me.id;
                        selectedUserID = Program.UserID;

                        browserLogin.Visible = false;

                        lblName.Text = Program.CurrentName;
                        imgFriendFace.ImageLocation = SelectedUserPictureURL;

                        Dictionary<string, string> userFriends = new Dictionary<string, string>();

                        dynamic myInfo = fbClient.Get(@"/me/taggable_friends");
                        var friends = new List<TaggableFriend>();
                        foreach (dynamic friend in myInfo.data)
                        {
                            userFriends.Add(friend.id, friend.name);
                            friends.Add(new TaggableFriend(friend.name, friend.id, friend.picture.data.url));
                        }
                        listFriends.DataSource = friends;
                        listFriends.DisplayMember = "name";
                        listFriends.ValueMember = "id";

                    }
                    else
                    {
                        MessageBox.Show("Couldn't log into Facebook!", "Login unsuccessful", MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                    }
                }
            }
        }

        public static string GetPictureUrl(string faceBookId)
        {
            WebResponse response = null;
            string pictureUrl = string.Empty;
            try
            {
                WebRequest request = WebRequest.Create(string.Format("https://graph.facebook.com/{0}/?fields=picture.type(large)", faceBookId));
                response = request.GetResponse();
                pictureUrl = response.ResponseUri.ToString();
            }
            catch (Exception ex)
            {
                //? handle
            }
            finally
            {
                if (response != null) response.Close();
            }
            return pictureUrl;
        }

        private void btnSwitch_Click(object sender, EventArgs e)
        {
            // change over ui elements to the user selected in the listbox
            this.selectedUserID = (listFriends.SelectedItem as TaggableFriend).ID;
            RefreshSelectedUser();
        }

        private void RefreshSelectedUser()
        {
            imgFriendFace.ImageLocation = SelectedUserPictureURL;
            dynamic friend = fbClient.Get(friendlyApiString);
            lblName.Text = friend.data[0].name;
        }

        private void RunPythonCommand(string cmd, string args)
        {
            string retaliationPath = Environment.CurrentDirectory + @"\Python\Retaliation\retaliation.py";
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = @"C:\Python27\pythonw.exe";
            start.Arguments = retaliationPath + string.Format(" {0} {1}", cmd, args);
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    Console.Write(result);
                }
            }
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            RunPythonCommand("up", cboMagnitude.SelectedItem.ToString());
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            RunPythonCommand("down", cboMagnitude.SelectedItem.ToString());
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            RunPythonCommand("left", cboMagnitude.SelectedItem.ToString());
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            RunPythonCommand("right", cboMagnitude.SelectedItem.ToString());
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            RunPythonCommand("reset", String.Empty);
        }

        private void btnFire_Click(object sender, EventArgs e)
        {
            RunPythonCommand("fire", String.Empty);
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            ReleaseData();
        }

        private void captureButtonClick(object sender, EventArgs e)
        {
            if (video != null)
            {
                if (isCapturing)
                {  //stop the capture
                    btnStart.Text = "Start Capture";
                    video.Pause();
                }
                else
                {
                    //start the capture
                    btnStart.Text = "Stop";
                    video.Start();
                }

                isCapturing = !isCapturing;
            }
        }

        private void ReleaseData()
        {
            if (video != null)
                video.Dispose();
        }

        private void btnTrain_Click(object sender, EventArgs e)
        {
            Train();
        }

        // declaration  of all variables, vectors and haarcascades

        Image<Bgr, Byte> currentFrame; //current image aquired from webcam for display
        Image<Gray, byte> result, TrainedFace = null; //used to store the result image and trained face
        Image<Gray, byte> gray_frame = null; //grayscale current image aquired from webcam for processing

        public CascadeClassifier FaceClassifier = new CascadeClassifier(Application.StartupPath + "/Cascades/haarcascade_frontalface_default.xml");//Our face detection method 

        //MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_COMPLEX, 0.5, 0.5); //Our fount for writing within the frame
        //int NumLabels;

        //Classifier with default training location
        ClassiferTrainer eigenTrainer = new ClassiferTrainer();

        public void RetrainClassifer()
        {
            eigenTrainer = new ClassiferTrainer();
            if (eigenTrainer.isTrained)
            {
                //lblTrainingInfo.Text = "Training Data loaded";
            }
            else
            {
                //lblTrainingInfo.Text = "No training data found, please train program using Train menu option";
            }
        }

        //Camera Start Stop
        public void InitializeVideoCapture()
        {
            video = new VideoCapture();
            video.QueryFrame();
            //Initialize the FrameGraber event
            //if (parrellelToolStripMenuItem.Checked)
            //{
            //    Application.Idle += new EventHandler(FrameGrabber_Parrellel);
            //}
            //else
            //{
            Application.Idle += new EventHandler(FrameGrabber_Standard);
            //}
        }

        private void StopVideoCapture()
        {
            //if (parrellelToolStripMenuItem.Checked)
            //{
            //    Application.Idle -= new EventHandler(FrameGrabber_Parrellel);
            //}
            //else
            //{
            Application.Idle -= new EventHandler(FrameGrabber_Standard);
            //}
            if (video != null)
            {
                video.Dispose();
            }
        }

        //Process Frame
        void FrameGrabber_Standard(object sender, EventArgs e)
        {
            imgCamera.Image = MyFaceRecognizer.FaceRecgonize(video.QueryFrame().ToImage<Bgr, Byte>().Resize(320, 240, Inter.Cubic), FaceClassifier, eigenTrainer);
        }

        void FrameGrabber_Parallel(object sender, EventArgs e)
        {
            //Get the current frame from capture device
            currentFrame = video.QueryFrame().ToImage<Bgr, Byte>().Resize(320, 240, Inter.Cubic);

            //Convert it to Grayscale
            ClearFoundFaces();

            if (currentFrame != null)
            {
                gray_frame = currentFrame.Convert<Gray, Byte>();
                //Face Detector
                Rectangle[] facesDetected = FaceClassifier.DetectMultiScale(gray_frame, 1.2, 10, new Size(50, 50), Size.Empty);

                //Action for each element detected
                Parallel.For(0, facesDetected.Length, i =>
                    {
                        try
                        {
                            facesDetected[i].X += (int)(facesDetected[i].Height * 0.15);
                            facesDetected[i].Y += (int)(facesDetected[i].Width * 0.22);
                            facesDetected[i].Height -= (int)(facesDetected[i].Height * 0.3);
                            facesDetected[i].Width -= (int)(facesDetected[i].Width * 0.35);

                            result = currentFrame.Copy(facesDetected[i]).Convert<Gray, byte>().Resize(100, 100, Inter.Cubic);
                            result._EqualizeHist();
                            //draw the face detected in the 0th (gray) channel with blue color
                            currentFrame.Draw(facesDetected[i], new Bgr(Color.Red), 2);

                            if (eigenTrainer.isTrained)
                            {
                                string name = eigenTrainer.Recognise(result);
                                int match_value = (int)eigenTrainer.eigenDistance;

                                //Draw the label for each face detected and recognized
                                currentFrame.Draw(name + " ", new Point(facesDetected[i].X - 2, facesDetected[i].Y - 2), FontFace.HersheyComplex, 12, new Bgr(Color.LightGreen));
                                AddFace(result, name, match_value);
                            }

                        }
                        catch
                        {
                            // error = no data, can continue
                        }
                    });
                //Show the faces procesed and recognized
                imgCamera.Image = currentFrame;
            }
        }

        //ADD Picture box and label to a panel for each face
        int faces_count = 0;
        int faces_panel_Y = 0;
        int faces_panel_X = 0;

        void ClearFoundFaces()
        {
            //this.Faces_Found_Panel.Controls.Clear();
            faces_count = 0;
            faces_panel_Y = 0;
            faces_panel_X = 0;
        }

        void AddFace(Image<Gray, Byte> img_found, string name_person, int match_value)
        {
            PictureBox PI = new PictureBox();
            PI.Location = new Point(faces_panel_X, faces_panel_Y);
            PI.Height = 80;
            PI.Width = 80;
            PI.SizeMode = PictureBoxSizeMode.StretchImage;
            PI.Image = img_found.ToBitmap();
            Label LB = new Label();
            LB.Text = name_person + " " + match_value.ToString();
            LB.Location = new Point(faces_panel_X, faces_panel_Y + 80);
            //LB.Width = 80;
            LB.Height = 15;

            //this.Faces_Found_Panel.Controls.Add(PI);
            //this.Faces_Found_Panel.Controls.Add(LB);
            //faces_count++;
            //if (faces_count == 2)
            //{
            //    faces_panel_X = 0;
            //    faces_panel_Y += 100;
            //    faces_count = 0;
            //}
            //else faces_panel_X += 85;

            //if (Faces_Found_Panel.Controls.Count > 10)
            //{
            //    Clear_Faces_Found();
            //}
        }

        //Menu Opeartions
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        private void singleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //parrellelToolStripMenuItem.Checked = false;
            //singleToolStripMenuItem.Checked = true;
            Application.Idle -= new EventHandler(FrameGrabber_Parallel);
            Application.Idle += new EventHandler(FrameGrabber_Standard);
        }
        private void parallelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //parrellelToolStripMenuItem.Checked = true;
            //singleToolStripMenuItem.Checked = false;
            Application.Idle -= new EventHandler(FrameGrabber_Standard);
            Application.Idle += new EventHandler(FrameGrabber_Parallel);

        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog SF = new SaveFileDialog();
            //As there is no identification in files to recogniser type we will set the extension ofthe file to tell us
            switch (eigenTrainer.recognizerType)
            {
                case ("EMGU.CV.LBPHFaceRecognizer"):
                    SF.Filter = "LBPHFaceRecognizer File (*.LBPH)|*.LBPH";
                    break;
                case ("EMGU.CV.FisherFaceRecognizer"):
                    SF.Filter = "FisherFaceRecognizer File (*.FFR)|*.FFR";
                    break;
                case ("EMGU.CV.EigenFaceRecognizer"):
                    SF.Filter = "EigenFaceRecognizer File (*.EFR)|*.EFR";
                    break;
            }
            if (SF.ShowDialog() == DialogResult.OK)
            {
                eigenTrainer.SaveEigenRecognizer(SF.FileName);
            }
        }
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog OF = new OpenFileDialog();
            OF.Filter = "EigenFaceRecognizer File (*.EFR)|*.EFR|LBPHFaceRecognizer File (*.LBPH)|*.LBPH|FisherFaceRecognizer File (*.FFR)|*.FFR";
            if (OF.ShowDialog() == DialogResult.OK)
            {
                eigenTrainer.LoadEigenRecognizer(OF.FileName);
            }
        }

        //Unknow Eigen face calibration
        //private void Eigne_threshold_txtbxChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Eigen_Recog.Set_Eigen_Threshold = Math.Abs(Convert.ToInt32(Eigne_threshold_txtbx.Text));
        //        message_bar.Text = "Eigen Threshold Set";
        //    }
        //    catch
        //    {
        //        message_bar.Text = "Error in Threshold input please use int";
        //    }
        //}

        private void Train()
        {
            //Stop Camera
            StopVideoCapture();

            //OpenForm
            TrainingForm TF = new TrainingForm(this);
            TF.Show();

            eigenTrainer.recognizerType = "EMGU.CV.EigenFaceRecognizer";
            eigenTrainer.Retrain();
        }

        private void btnTrainFriend_Click(object sender, EventArgs e)
        {
            var result = MyFaceRecognizer.DetectFace(new Image<Bgr, Byte>(new Bitmap(imgFriendFace.Image)).Resize(320, 240, Inter.Cubic), FaceClassifier, pbxGrayFriend, false);
            if (result != null)
                MyFaceRecognizer.SaveTrainingData(pbxGrayFriend.Image, lblName.Text);
        }

        //private void InitializeComponent()
        //{
        //    this.SuspendLayout();
        //    // 
        //    // MainForm
        //    // 
        //    this.ClientSize = new System.Drawing.Size(284, 261);
        //    this.Name = "MainForm";
        //    this.ResumeLayout(false);

        //}

        //private void eigenToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    //Uncheck other menu items
        //    fisherToolStripMenuItem.Checked = false;
        //    lBPHToolStripMenuItem.Checked = false;

        //    Eigen_Recog.Recognizer_Type = "EMGU.CV.EigenFaceRecognizer";
        //    Eigen_Recog.Retrain();
        //}

        //private void fisherToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    //Uncheck other menu items
        //    lBPHToolStripMenuItem.Checked = false;
        //    eigenToolStripMenuItem.Checked = false;

        //    Eigen_Recog.Recognizer_Type = "EMGU.CV.FisherFaceRecognizer";
        //    Eigen_Recog.Retrain();
        //}

        //private void lBPHToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    //Uncheck other menu items
        //    fisherToolStripMenuItem.Checked = false;
        //    eigenToolStripMenuItem.Checked = false;

        //    Eigen_Recog.Recognizer_Type = "EMGU.CV.LBPHFaceRecognizer";
        //    Eigen_Recog.Retrain();
        //}
    }
}
