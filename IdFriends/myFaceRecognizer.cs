﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Emgu.CV.UI;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;

using System.IO;
using System.Drawing.Imaging;
using System.Xml;
using System.Threading;

namespace IdFriends
{
    /// <summary>
    /// Face Recognizer
    /// </summary>
    public static class MyFaceRecognizer
    {
        //private static Emgu.CV.Font font = new MCvFont(FONT.CV_FONT_HERSHEY_COMPLEX, 0.5, 0.5); //Our fount for writing within the frame

        public static Bitmap DetectFace(Image<Bgr, Byte> frameToGrab, CascadeClassifier Face, PictureBox grayOutput, bool BulkRecording)
        {
            // Get the current frame from video capture
            var currentFrame = frameToGrab;

            // Convert it to Grayscale
            if (currentFrame != null)
            {
                var grayFrame = currentFrame.Convert<Gray, Byte>();

                //Face Detector
                //MCvAvgComp[][] facesDetected = gray_frame.DetectHaarCascade(Face, 1.2, 10, Emgu.CV.CvEnum.HAAR_DETECTION_TYPE.DO_CANNY_PRUNING, new Size(20, 20)); //old method
                Rectangle[] facesDetected = Face.DetectMultiScale(grayFrame, 1.2, 10, new Size(50, 50), Size.Empty);

                //Action for each element detected
                for (int i = 0; i < facesDetected.Length; i++)// (Rectangle face_found in facesDetected)
                {
                    //This will focus in on the face from the haar results its not perfect but it will remove a majoriy
                    //of the background noise
                    facesDetected[i].X += (int)(facesDetected[i].Height * 0.15);
                    facesDetected[i].Y += (int)(facesDetected[i].Width * 0.22);
                    facesDetected[i].Height -= (int)(facesDetected[i].Height * 0.3);
                    facesDetected[i].Width -= (int)(facesDetected[i].Width * 0.35);

                    var result = currentFrame.Copy(facesDetected[i]).Convert<Gray, byte>().Resize(100, 100, Inter.Cubic);
                    result._EqualizeHist();
                    grayOutput.Image = result.ToBitmap();
                    //draw the face detected in the 0th (gray) channel with a color
                    currentFrame.Draw(facesDetected[i], new Bgr(Color.Red), 2);
                }

                // handle bulk recording
                //if (BulkRecording && facesDetected.Length > 0 && resultImages.Count < num_faces_to_aquire)
                //{
                //    resultImages.Add(result);
                //    count_lbl.Text = "Count: " + resultImages.Count.ToString();
                //    if (resultImages.Count == num_faces_to_aquire)
                //    {
                //        ADD_BTN.Enabled = true;
                //        NEXT_BTN.Visible = true;
                //        PREV_btn.Visible = true;
                //        count_lbl.Visible = false;
                //        Single_btn.Visible = true;
                //        ADD_ALL.Visible = true;
                //        RECORD = false;
                //        Application.Idle -= new EventHandler(FrameGrabber);
                //    }
                //}
                return currentFrame.ToBitmap();
            }
            return null;
        }

        //Process Frame
        public static Image<Bgr, Byte> FaceRecgonize(Image<Bgr, Byte> frameToGrab, CascadeClassifier Face, ClassiferTrainer EigenRegcognizer)
        {
            //Get the current frame form capture device
            var currentFrame = frameToGrab;
            Image<Gray, Byte> grayFrame;

            //Convert it to Grayscale
            if (currentFrame != null)
            {
                grayFrame = currentFrame.Convert<Gray, Byte>();

                //Face Detector
                Rectangle[] facesDetected = Face.DetectMultiScale(grayFrame, 1.2, 10, new Size(50, 50), Size.Empty);

                //Action for each element detected
                for (int i = 0; i < facesDetected.Length; i++)// (Rectangle face_found in facesDetected)
                {
                    //This will focus in on the face from the haar results its not perfect but it will remove a majoriy
                    //of the background noise
                    facesDetected[i].X += (int)(facesDetected[i].Height * 0.15);
                    facesDetected[i].Y += (int)(facesDetected[i].Width * 0.22);
                    facesDetected[i].Height -= (int)(facesDetected[i].Height * 0.3);
                    facesDetected[i].Width -= (int)(facesDetected[i].Width * 0.35);

                    var result = currentFrame.Copy(facesDetected[i]).Convert<Gray, byte>().Resize(100, 100, Inter.Cubic);
                    result._EqualizeHist();
                    //draw the face detected in the 0th (gray) channel with blue color
                    currentFrame.Draw(facesDetected[i], new Bgr(Color.Red), 2);

                    if (EigenRegcognizer.isTrained)
                    {
                        string name = EigenRegcognizer.Recognise(result);
                        int match_value = (int)EigenRegcognizer.eigenDistance;

                        //Draw the label for each face detected and recognized
                        currentFrame.Draw(name + " ",new Point(facesDetected[i].X - 2, facesDetected[i].Y - 2), FontFace.HersheyComplex, 24, new Bgr(Color.LightGreen));
                        //AddFace(result, name, match_value);
                    }
                }
            }
            return currentFrame; // Show the faces procesed and recognized
        }

        //Saving The Data
        public static bool SaveTrainingData(Image face_data, string PersonName)
        {
            try
            {
                Random rand = new Random();
                bool file_create = true;
                string facename = "face_" + PersonName + "_" + rand.Next().ToString() + ".jpg";
                while (file_create)
                {

                    if (!File.Exists(Application.StartupPath + "/TrainedFaces/" + facename))
                    {
                        file_create = false;
                    }
                    else
                    {
                        facename = "face_" + PersonName + "_" + rand.Next().ToString() + ".jpg";
                    }
                }


                if (Directory.Exists(Application.StartupPath + "/TrainedFaces/"))
                {
                    face_data.Save(Application.StartupPath + "/TrainedFaces/" + facename, ImageFormat.Jpeg);
                }
                else
                {
                    Directory.CreateDirectory(Application.StartupPath + "/TrainedFaces/");
                    face_data.Save(Application.StartupPath + "/TrainedFaces/" + facename, ImageFormat.Jpeg);
                }
                if (File.Exists(Application.StartupPath + "/TrainedFaces/TrainedLabels.xml"))
                {
                    XmlDocument FacesXML = new XmlDocument();
                    //File.AppendAllText(Application.StartupPath + "/TrainedFaces/TrainedLabels.txt", NAME_PERSON.Text + "\n\r");
                    bool loading = true;
                    while (loading)
                    {
                        try
                        {
                            FacesXML.Load(Application.StartupPath + "/TrainedFaces/TrainedLabels.xml");
                            loading = false;
                        }
                        catch
                        {
                            FacesXML = null;
                            FacesXML = new XmlDocument();
                            Thread.Sleep(10);
                        }
                    }

                    //Get the root element
                    XmlElement root = FacesXML.DocumentElement;

                    XmlElement face_D = FacesXML.CreateElement("FACE");
                    XmlElement name_D = FacesXML.CreateElement("NAME");
                    XmlElement file_D = FacesXML.CreateElement("FILE");

                    //Add the values for each nodes
                    //name.Value = textBoxName.Text;
                    //age.InnerText = textBoxAge.Text;
                    //gender.InnerText = textBoxGender.Text;
                    name_D.InnerText = PersonName;
                    file_D.InnerText = facename;

                    //Construct the Person element
                    //person.Attributes.Append(name);
                    face_D.AppendChild(name_D);
                    face_D.AppendChild(file_D);

                    //Add the New person element to the end of the root element
                    root.AppendChild(face_D);

                    //Save the document
                    FacesXML.Save(Application.StartupPath + "/TrainedFaces/TrainedLabels.xml");
                    //XmlElement child_element = docu.CreateElement("FACE");
                    //docu.AppendChild(child_element);
                    //docu.Save("TrainedLabels.xml");
                }
                else
                {
                    FileStream FS_Face = File.OpenWrite(Application.StartupPath + "/TrainedFaces/TrainedLabels.xml");
                    using (XmlWriter writer = XmlWriter.Create(FS_Face))
                    {
                        writer.WriteStartDocument();
                        writer.WriteStartElement("Faces_For_Training");

                        writer.WriteStartElement("FACE");
                        writer.WriteElementString("NAME", PersonName);
                        writer.WriteElementString("FILE", facename);
                        writer.WriteEndElement();

                        writer.WriteEndElement();
                        writer.WriteEndDocument();
                    }
                    FS_Face.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
